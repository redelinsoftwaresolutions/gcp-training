# infrastructure

The idea is to be able to create a development environment from scratch by just using this repository. Also, to create the Cloud infrastructure from scratch with this repository.

# Installation 
* Install and setup [Vagrant](https://www.vagrantup.com/downloads.html)
* Install and setup [Virtualbox](https://www.virtualbox.org/wiki/Downloads)

# Create development environment
* change to a directory of your choice
* clone infrastructure repository
```
git clone https://bitbucket.org/redelinsoftwaresolutions/gcp-training.git
```
* change to ./infrastructure
* fire up vagrant
```
vagrant plugin install vagrant-disksize
vagrant up
```
* Connect to the VM
```
vagrant ssh
```
* The now executing startup script helps you to setup your environment
## Unguided tool setup
If the startup script didn't work, here's the information on how to do it manually.
You can also have a look at /home/vagrant/.bashrc_local to see what the script usually 
does on login.
* Initialize gcloud
```
gcloud auth login
```
* add gcloud credentials for project from secrets.kdbx entry "Terraform service account"
If directory .config or gcloud doesn't exist, you didn't initialize gcloud.
```
vim ~/.config/gcloud/terraform-admin.json
```
* Activate Terraform service account in gcloud
```
gcloud auth activate-service-account terraform@training-221520.iam.gserviceaccount.com --key-file ${TF_CREDS}
```
* generate SSH key
```
ssh-keygen
cat .ssh/id_rsa.pub
```
* add ssh key to your user at bitbucket for using the command line https://bitbucket.org/account/user/<yourbitbucketusername>/ssh-keys/

# Recreate Terraform user (you probably won't ever need that)
* Execute 
```
./setupTerraform.sh
```
# Create infrastructure
```
cd /git/gcp-training/
terraform init
terraform plan
terraform apply
```
or use prepared script
```
./applyTerraform.sh
```
