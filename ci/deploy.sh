#!/bin/sh

BASE_DIR=$(dirname "$0")

cd $BASE_DIR
cd ../10_combinationlock

if [ "$1" = "local" ]; then
    echo "local execution"
    # expose terraform environment variables
    export TF_VAR_MONGO_URL=$MONGO_URL
    export TF_VAR_MONGO_PASS_READWRITE=$(cat $MONGO_PW_READWRITE)
    export TF_VAR_MONGO_USER_READWRITE=$MONGO_USER_READWRITE
fi

terraform init \
&& terraform apply -auto-approve -lock=false # temporarily ignore lock due to config error
