#!/bin/bash

# add repository for gcloud
export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -   
sudo echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" > /etc/apt/sources.list.d/google-cloud-sdk.list

# add repository for nodejs 10
curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -
sudo rm /etc/apt/sources.list.d/nodesource.list
sudo echo 'deb https://deb.nodesource.com/node_10.x bionic main' > /etc/apt/sources.list.d/nodesource.list
sudo echo 'deb-src https://deb.nodesource.com/node_10.x bionic main' >> /etc/apt/sources.list.d/nodesource.list

# add repository for mongodb shell
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
sudo echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/4.0 multiverse" > /etc/apt/sources.list.d/mongodb-org-4.0.list

# install packages
sudo apt-get -q=2 -o=Dpkg::Use-Pty=0 update && apt-get -y -q=2 -o=Dpkg::Use-Pty=0 upgrade
sudo apt-get install -y -q=2 -o=Dpkg::Use-Pty=0 git-lfs python3 python3-pip nodejs unzip wget google-cloud-sdk git vim gcc g++ make mongodb-org-shell default-jdk jq

# install terraform
TF_BASE="https://releases.hashicorp.com/terraform"
TF_VERSION=$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')
TF_FILE=terraform_${TF_VERSION}_linux_amd64.zip
wget -q "$TF_BASE/$TF_VERSION/$TF_FILE"
unzip -q ${TF_FILE}
apt-get -qqy -q=2 -o=Dpkg::Use-Pty=0 remove unzip jq
mv terraform /usr/local/bin/
rm terraform_*
echo "Installed Terraform."

# clean up apt-get caches
sudo apt-get -q=2 -o=Dpkg::Use-Pty=0 -y autoclean && sudo apt-get -q=2 -o=Dpkg::Use-Pty=0 -y clean && sudo apt-get -q=2 -o=Dpkg::Use-Pty=0 -y autoremove

# install awscli
# pip3 install awscli --upgrade

# set environment variables
export TF_ADMIN=terraform
export JAVA_HOME=$(ls -d /usr/lib/jvm/java-* | head -1)
export TF_CREDS=/home/vagrant/.config/gcloud/terraform-admin.json
export TF_VAR_project_name=training-221520
export TF_VAR_region=europe-west1
#export TF_VAR_org_id=697603851999
#export TF_VAR_billing_account=01E4AD-02528C-03729B
export GOOGLE_APPLICATION_CREDENTIALS=${TF_CREDS}
export GOOGLE_PROJECT=${TF_ADMIN}
export MONGO_URL="mongodb://XUSERX:XPASSWORDX@redelinsoftcluster-shard-00-00-nnjdp.gcp.mongodb.net:27017,redelinsoftcluster-shard-00-01-nnjdp.gcp.mongodb.net:27017,redelinsoftcluster-shard-00-02-nnjdp.gcp.mongodb.net:27017/test?ssl=true&replicaSet=RedelinSoftCluster-shard-0&authSource=admin&retryWrites=true"
export MONGO_PW_READWRITE=/home/vagrant/.config/gcloud/mongodbpw_readwrite
export MONGO_USER_READWRITE=redelinsoft-functions-write

# make sure they are available at each startup
rm /home/vagrant/.bashrc_local
sudo cat > /home/vagrant/.bashrc_local <<EOF
export JAVA_HOME=${JAVA_HOME}
export TF_ADMIN=${TF_ADMIN}
export TF_CREDS=${TF_CREDS}
export TF_VAR_project_name=${TF_VAR_project_name}
export TF_VAR_region=${TF_VAR_region}
export GOOGLE_APPLICATION_CREDENTIALS=${TF_CREDS}
export GOOGLE_PROJECT=${TF_ADMIN}
export MONGO_URL='${MONGO_URL}'
export MONGO_PW_READWRITE=${MONGO_PW_READWRITE}
export MONGO_USER_READWRITE=${MONGO_USER_READWRITE}
if [ ! -f /home/vagrant/.ssh/id_rsa ]; then
    echo "Generating new key..."
    ssh-keygen -t rsa -b 4096 -f /home/vagrant/.ssh/id_rsa
    echo "Generated new key! Your key is:"
    cat /home/vagrant/.ssh/id_rsa.pub
    echo "###### Please add key now to Bitbucket. I'll wait..."
    sleep 10
    read -p "Ready to continue? " -n 1 -r
fi

if [ ! -d /home/vagrant/.config/gcloud ]; then
    echo "Now configuring gcloud..."
    sleep 5
    gcloud auth login
fi 

if [ ! -f ${TF_CREDS} ]; then
    echo "###### Please add gcloud credentials to ${TF_CREDS} ######"
    sleep 5
    vim ${TF_CREDS}
fi

if [ -f ${TF_CREDS} ] && [ ! -f ${TF_CREDS}.serviceaccountactivated ]; then
    gcloud auth activate-service-account terraform@training-221520.iam.gserviceaccount.com --key-file ${TF_CREDS}
    gcloud config set project ${TF_VAR_project_name}
    touch ${TF_CREDS}.serviceaccountactivated
fi

if [ ! -f ${MONGO_PW_READWRITE} ]; then
    echo "###### Please add mongodb readwrite credentials ######"
    sleep 5
    vim ${MONGO_PW_READWRITE}
fi

git config --global core.autocrlf input
cd /git/gcp-training
if ! grep -q lfs "/home/vagrant/.gitconfig" ; then
	git lfs install
fi

GIT_PROMPT_ONLY_IN_REPO=1
source ~/.bash-git-prompt/gitprompt.sh

# set starting directory
cd /git

EOF
chown vagrant:vagrant .bashrc_local

cat /etc/skel/.bashrc > /home/vagrant/.bashrc
echo ". /home/vagrant/.bashrc_local" >> /home/vagrant/.bashrc

# set timezone
sudo ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime
sudo dpkg-reconfigure -f noninteractive tzdata

# add aliases
sudo cat > /home/vagrant/.bash_aliases <<EOF
alias tf='/usr/local/bin/terraform'
EOF
chown vagrant:vagrant .bash_aliases

# git stuff
cd /git/gcp-training 
cd /home/vagrant 
rm -rf .bash-git-prompt
git clone -q https://github.com/magicmonty/bash-git-prompt.git .bash-git-prompt --depth=1
chown -R vagrant:vagrant .bash-git-prompt