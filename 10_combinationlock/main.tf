terraform {
 backend "gcs" {
   bucket  = "redelinsoft-training-terraform-state"
   prefix = "function/combinationlock"
   project = "redelinsoft-training-terraform-state"
 }
}

provider "google" {
 # credentials = "${file("~/.config/gcloud/terraform-admin.json")}"
 project = "${local.project_id}"
 region = "${local.region}"
}