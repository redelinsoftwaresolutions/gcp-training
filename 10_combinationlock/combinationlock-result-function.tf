data "archive_file" "init-combinationlock-result-function" {
  type        = "zip"
  source_dir = "./combinationlock-result-function"
  output_path = "./combinationlock-result-function.zip"
}

resource "google_storage_bucket_object" "archive-combinationlock-result-function" {
  # name manipulation is necessary to redeploy the function with source.
  # else gcp will not recognize the changed code.
  name   = "combinationlock-result-function-${lower(replace(timestamp(), ":", ""))}.zip"
  bucket = "${local.functionBucket}"
  source = "./combinationlock-result-function.zip"
}

resource "google_cloudfunctions_function" "function-combinationlock-result" {
  name                  = "combinationlock-result-function"
  description           = "Function to prepare a combination lock to hack for training"
  project               = "${local.project_id}"
  available_memory_mb   = 128
  source_archive_bucket = "${local.functionBucket}"
  source_archive_object = "${google_storage_bucket_object.archive-combinationlock-result-function.name}"
  trigger_http          = true
  timeout               = 60
  #runtime               = "nodejs8"
  entry_point           = "open"

  lifecycle {
    create_before_destroy = true
  }
  labels {
    function-name = "combinationlock-result-function"
  }

  environment_variables {
    VERSION = "${lower(replace(timestamp(), ":", ""))}"
    MONGO_URL = "${var.MONGO_URL}"
    MONGO_USER = "${var.MONGO_USER_READWRITE}"
    MONGO_PASS = "${var.MONGO_PASS_READWRITE}"
  }
}