'use strict';
const fs = require('fs');
const mongoClient = require('mongodb').MongoClient;
var padStart = require('string.prototype.padstart');

const version = process.env.VERSION;
const codeLevel1 = padStart(process.env.CODE_LEVEL_1, 3, '0');
const codeLevel2 = padStart(process.env.CODE_LEVEL_2, 4, '0');
var mongoUri = process.env.MONGO_URL.replace("XPASSWORDX", process.env.MONGO_PASS).replace("XUSERX", process.env.MONGO_USER);

function readFileToString(file) {
  return fs.readFileSync(file, 'utf8').toString();
}

function handleGetRequest(req, res) {
  if (req.query.resource !== undefined) {
    handleGetRequestForResource(req, res);
  } else {
    handleGetRequestForPage(req, res);
  }
}

function handleGetRequestForResource(req, res) {
  var resourcePath = "view/" + req.query.resource;

  if (req.query.resource.endsWith(".js")) {
    res.setHeader('Content-Type', 'application/javascript');
    res.status(200).send(readFileToString(resourcePath));
  } else if (req.query.resource.endsWith(".css")) {
    res.setHeader('Content-Type', 'text/css');
    res.status(200).send(readFileToString(resourcePath));
  } else {
    res.status(400).send(JSON.stringify({ errormessage: "unknown resource type" }));
  }
}

function handleGetRequestForPage(req, res) {
  var numberFieldDisplayTemplate = readFileToString('view/numberFieldDisplay.html');
  var numberFieldInputTemplate = readFileToString('view/numberFieldInput.html');
  var contentTemplate = readFileToString('view/thelock.html');

  var numberFieldDisplay = "";
  var numberFieldInput = "";
  var content = contentTemplate;
  if (req.query.level !== undefined && req.query.level == 2) {
    // LEVEL 2
    for (var i = 0; i < 4; i++) {
      numberFieldDisplay += numberFieldDisplayTemplate.replace(/numberXXX/g, 'number' + i);
      numberFieldInput += numberFieldInputTemplate.replace(/numberXXX/g, 'number' + i);
    }
    content = content.replace(/--level-input--/g, "2");
    content = content.replace(/--level--/g, "style.addition-level2.css");
  } else {
    // LEVEL 1
    for (var i = 0; i < 3; i++) {
      numberFieldDisplay += numberFieldDisplayTemplate.replace(/numberXXX/g, 'number' + i);
      numberFieldInput += numberFieldInputTemplate.replace(/numberXXX/g, 'number' + i);
    }
    content = content.replace(/--level-input--/g, "1");
    content = content.replace(/--level--/g, "style.addition-level1.css");
  }

  content = content.replace('<NumberFieldDisplay />', numberFieldDisplay);
  content = content.replace('<NumberFieldInput />', numberFieldInput);

  res.setHeader('Content-Type', 'text/html');
  res.status(200).send(content);
}

function handlePostRequest(req, res) {
  if (req.body.teamname === undefined || req.body.teamname.length == 0) {
    res.status(400).send(JSON.stringify({ errormessage: "please enter a teamname" }));
    return;
  }

  console.log("body is: " + JSON.stringify(req.body));

  if (req.body.level !== undefined && req.body.level == 2) {
    if (req.body.number0 === undefined ||
      req.body.number1 === undefined ||
      req.body.number2 === undefined ||
      req.body.number3 === undefined) {
      saveRequestResult(res, req.body.teamname, false, req.body.level, () => {
        res.status(400).send(JSON.stringify({ errormessage: 'missing number field' }));
      });
    } else {
      if (req.body.number0 + req.body.number1 + req.body.number2 + req.body.number3 == codeLevel2) {
        saveRequestResult(res, req.body.teamname, true, req.body.level, () => {
          res.status(200).send(JSON.stringify({ info: "Congratulations! You are in the castle!" }));
        });
      } else {
        saveRequestResult(res, req.body.teamname, false, req.body.level, () => {
          res.status(403).send(JSON.stringify({ errormessage: "wrong code" }));
        });
      }
    }
  } else {
    if (req.body.number0 === undefined ||
      req.body.number1 === undefined ||
      req.body.number2 === undefined) {
      saveRequestResult(res, req.body.teamname, false, 1, () => {
        res.status(400).send(JSON.stringify({ errormessage: 'missing number field' }));
      });
    } else {
      if (req.body.number0 + req.body.number1 + req.body.number2 == codeLevel1) {
        saveRequestResult(res, req.body.teamname, true, 1, () => {
          res.status(200).send(JSON.stringify({ info: "Congratulations! You are in the castle!" }));
        });
      } else {
        saveRequestResult(res, req.body.teamname, false, 1, () => {
          res.status(403).send(JSON.stringify({ errormessage: "wrong code" }));
        });
      }
    }
  }
}

function saveRequestResult(res, teamname, isSuccess, level, callback) {
  mongoClient.connect(mongoUri, { useNewUrlParser: true })
    .then(conn => {
      return conn.db("combinationLock").collection("teamrequest")
        .find({ "teamname": teamname }).toArray()
        .then(result => {
          var col = conn.db("combinationLock").collection("teamrequest");
          console.log("entries (" + result.length + ") found for teamname: " + teamname);
          if (result.length > 0) {
            console.log("update teamrequest: " + JSON.stringify(result[0]));
            updateTeamrequest(col, result[0], isSuccess, level);
          } else {
            console.log("insert teamrequest for team: " + teamname);
            insertTeamrequest(col, teamname, isSuccess, level);
          }
        })
        .then(() => conn.close())
        .then(() => callback())
    })
    .catch(error => {
      console.error("error while fetching teamrequest. error: " + error, error.stack.split("\n"));
      res.status(400).send(JSON.stringify({
        'error': 'read failed',
        'error_description': "reading on teamrequest failed due to error: " + error
      }));
    });
}

function insertTeamrequest(collection, teamname, isSuccess, level) {
  collection.insertOne({
    "teamname": teamname,
    "tries": 1,
    "level" : level,
    "success": isSuccess,
    "firstRequest": Date.now(),
    "lastRequest": Date.now()
  })
}

function updateTeamrequest(collection, teamrequest, isSuccess, level) {
  collection.updateOne({ "teamname": teamrequest.teamname },
    {
      $set: {
        "tries": teamrequest.tries + 1,
        "success": isSuccess,
        "level": level,
        "lastRequest": Date.now()
      }
    });
}

exports.open = (req, res) => {
  console.log("Request with method: " + req.method);
  res.setHeader('Access-Control-Allow-Origin', "*");
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.setHeader('Access-Control-Max-Age', 3600);
  res.setHeader('X-Content-Type-Options', 'nosniff');
  res.setHeader('X-Version', version);
  // default content-type is application/json, else it would be overwritten
  res.setHeader('Content-Type', 'application/json');

  switch (req.method) {
    case 'OPTIONS':
      // CORS related requests should have valid response
      res.status(204).send('');
      break;
    case 'GET':
      handleGetRequest(req, res);
      break;
    case 'POST':
      handlePostRequest(req, res);
      break;
    default:
      console.log("invalid request method: " + req.method);
      res.status(400).send(JSON.stringify({
        'error': 'invalid_request',
        'error_description': 'Request type not supported.'
      }));
  }
}