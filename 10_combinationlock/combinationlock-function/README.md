# Training: Combination Lock Hacking experience
## general conditions
* suitable for children and teenagers between 11 and 18 years of age  
* keine Vorkenntnisse in Programmierung notwendig  
* no previous knowledge in programming necessary  
* Only one device with a browser and keyboard required  
* Works in teams of two. This is more fun.  
## the challenge
This training should show you the comparability of the real world with the digital world.  
The goal is to crack the combination lock.
### step 1
Take a real combination lock and try to crack it. Remember which ideas and algorithms you used.
### step 2
Now crack the digital combination lock. The ideas and algorithms from Step 1 will help you!

## how to?
### links
* The digital combination lock (Level 1): https://europe-west1-training-221520.cloudfunctions.net/combinationlock-function
* The digital combination lock (Level 2): https://europe-west1-training-221520.cloudfunctions.net/combinationlock-function?level=2
* Evaluation of the previous hacker teams: https://europe-west1-training-221520.cloudfunctions.net/combinationlock-result-function
### procedure
Use the W3C Try-it Web Editor to program your hack: https://www.w3schools.com/html/tryit.asp?filename=tryhtml_basic  
Remember to save your code regularly and remember the temporary URL. Otherwise you will lose your developed code.  
A very helpful JavaScript library is jQuery. You can embed it in your HTML as follows.
```
<html>
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </head>
  <body>
    ...
  </body>
</html>
```
  
*Attention:* The training now includes an explanation of the following topics so that you can start developing.  
1. Website all well and good, but what is behind it (show source code)?  
2. Which elements can be found in the Combination Lock website?  
3. How does the Combination Lock website work?  
On site we will help you to find the right entry point and to enable your team to hack.  
  
Now program your hacking code, which automatically cracks the combination lock :)