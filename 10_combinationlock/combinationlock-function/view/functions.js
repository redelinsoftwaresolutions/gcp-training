function transform(fromFieldId, toFieldId) {
  $('#'+toFieldId).val($('#'+fromFieldId).val());
}

function increase(fieldId) {
  var value = $('#' + fieldId).val();
  if (value < 9) {
    value++;
  } else {
    value = 0;
  }
  $('#' + fieldId).val(value);
  $('#display_' + fieldId).text($('#' + fieldId).val());
}

function serializeForm(formId) {
  var object = {};
  $(formId).serializeArray().forEach(function (value) {
    object[value.name] = value.value;
  });
  var json = JSON.stringify(object);
  console.log("form-data-json: " + json);
  return json;
}

function sendLockCombination(url, formData) {
  $('#disable').show();
  $.ajax({
    type: 'POST',
    url: url,
    contentType: 'application/json; charset=utf-8',
    data: formData,
    success: function (result) {
      $('#errormessage').hide();
      $('#success').show();
      setTimeout(function () {
        $('#success').fadeOut('fast');
      }, 10000);
    },
    error: function (err) {
      $('#errormessage').text(JSON.stringify(err));
      $('#errormessage').show();
    }
  })
    .always(function () {
      $('#disable').hide();
    });
}