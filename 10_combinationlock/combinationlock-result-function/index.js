'use strict';
const fs = require('fs');
const mongoClient = require('mongodb').MongoClient;
const moment = require('moment');

const version = process.env.VERSION;
var mongoUri = process.env.MONGO_URL.replace("XPASSWORDX", process.env.MONGO_PASS).replace("XUSERX", process.env.MONGO_USER);

function readFileToString(file) {
  return fs.readFileSync(file, 'utf8').toString();
}

function handleGetRequest(req, res) {
  // remove data which is older than 1 day by async function without callback
  removeOldData();

  if (req.query.resource !== undefined) {
    handleGetRequestForResource(req, res);
  } else {
    handleGetRequestForPage(req, res);
  }
}

function handleGetRequestForResource(req, res) {
  var resourcePath = "view/" + req.query.resource;

  if (req.query.resource.endsWith(".js")) {
    res.setHeader('Content-Type', 'application/javascript');
    res.status(200).send(readFileToString(resourcePath));
  } else if (req.query.resource.endsWith(".css")) {
    res.setHeader('Content-Type', 'text/css');
    res.status(200).send(readFileToString(resourcePath));
  } else {
    res.status(400).send(JSON.stringify({ errormessage: "unknown resource type" }));
  }
}

function handleGetRequestForPage(req, res) {
  const TIMEFORMAT = 'YYYY-MM-DD HH:mm:ss';
  var resultDetailTemplate = readFileToString('view/result-detail.html');
  var contentTemplate = readFileToString('view/result.html');

  var resultDetail = "";
  var content = contentTemplate;
  fetchSortedResults(result => {
    result.forEach(element => {
      var tmpResultDetail = resultDetailTemplate.replace(/teamname/g, element.teamname);
      tmpResultDetail = tmpResultDetail.replace(/level/g, element.level);
      tmpResultDetail = tmpResultDetail.replace(/hackStart/g, moment(element.firstRequest).format(TIMEFORMAT));
      tmpResultDetail = tmpResultDetail.replace(/lastHack/g, moment(element.lastRequest).format(TIMEFORMAT));
      tmpResultDetail = tmpResultDetail.replace(/tries/g, element.tries);
      tmpResultDetail = tmpResultDetail.replace(/isSuccess/g, element.success);
      resultDetail += tmpResultDetail;
    });
    content = content.replace('<ResultDetail />', resultDetail);

    res.setHeader('Content-Type', 'text/html');
    res.status(200).send(content);
  });
}

function fetchSortedResults(callback) {
  mongoClient.connect(mongoUri, { useNewUrlParser: true })
    .then(conn => {
      return conn.db("combinationLock").collection("teamrequest")
        .find().sort({ success: -1, lastRequest: -1 }).toArray()
        .then(result => {
          console.log("raw result is: " + JSON.stringify(result));
          callback(result);
        })
        .then(() => conn.close())
    })
    .catch(error => {
      console.error("error while fetching teamrequests. error: " + error, error.stack.split("\n"));
      res.status(400).send(JSON.stringify({
        'error': 'read failed',
        'error_description': "reading on teamrequests failed due to error: " + error
      }));
    });
}

function removeOldData() {
  var removeDate = moment();
  removeDate = removeDate.subtract(1, "days").valueOf();
  console.log("delete data older than: " + removeDate);

  mongoClient.connect(mongoUri, { useNewUrlParser: true })
    .then(conn => {
      conn.db("combinationLock").collection("teamrequest")
        .deleteMany({"firstRequest": {$lt: removeDate}}, (err, collection) => {
          if (err) throw err;
          console.log(collection.result.n + " Record(s) deleted successfully");
        })
      return conn;
    })
    .then(conn => conn.close())
    .catch(error => {
      console.error("error while removing old teamrequests. error: " + error, error.stack.split("\n"));
    });
}

exports.open = (req, res) => {
  console.log("Request with method: " + req.method);
  res.setHeader('Access-Control-Allow-Origin', "*");
  res.setHeader('Access-Control-Allow-Methods', 'GET');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.setHeader('Access-Control-Max-Age', 3600);
  res.setHeader('X-Content-Type-Options', 'nosniff');
  res.setHeader('X-Version', version);
  // default content-type is application/json, else it would be overwritten
  res.setHeader('Content-Type', 'application/json');

  switch (req.method) {
    case 'OPTIONS':
      // CORS related requests should have valid response
      res.status(204).send('');
      break;
    case 'GET':
      handleGetRequest(req, res);
      break;
    default:
      console.log("invalid request method: " + req.method);
      res.status(400).send(JSON.stringify({
        'error': 'invalid_request',
        'error_description': 'Request type not supported.'
      }));
  }
}