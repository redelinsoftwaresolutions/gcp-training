resource "random_integer" "code-level1" {
  min     = 1
  max     = 999
  keepers = {
    time = "${lower(replace(timestamp(), ":", ""))}"
  }
}

resource "random_integer" "code-level2" {
  min     = 1
  max     = 9999
  keepers = {
    time = "${lower(replace(timestamp(), ":", ""))}"
  }
}

data "archive_file" "init-combinationlock-function" {
  type        = "zip"
  source_dir = "./combinationlock-function"
  output_path = "./combinationlock-function.zip"
}

resource "google_storage_bucket_object" "archive-combinationlock-function" {
  # name manipulation is necessary to redeploy the function with source.
  # else gcp will not recognize the changed code.
  name   = "combinationlock-function-${lower(replace(timestamp(), ":", ""))}.zip"
  bucket = "${local.functionBucket}"
  source = "./combinationlock-function.zip"
}

resource "google_cloudfunctions_function" "function-combinationlock" {
  name                  = "combinationlock-function"
  description           = "Function to prepare a combination lock to hack for training"
  project               = "${local.project_id}"
  available_memory_mb   = 128
  source_archive_bucket = "${local.functionBucket}"
  source_archive_object = "${google_storage_bucket_object.archive-combinationlock-function.name}"
  trigger_http          = true
  timeout               = 30
  #runtime               = "nodejs8"
  entry_point           = "open"

  lifecycle {
    create_before_destroy = true
  }
  labels {
    function-name = "combinationlock-function"
  }

  environment_variables {
    VERSION = "${lower(replace(timestamp(), ":", ""))}"
    CODE_LEVEL_1 = "${random_integer.code-level1.result}"
    //CODE_LEVEL_1 = "023"
    CODE_LEVEL_2 = "${random_integer.code-level2.result}"
    MONGO_URL = "${var.MONGO_URL}"
    MONGO_USER = "${var.MONGO_USER_READWRITE}"
    MONGO_PASS = "${var.MONGO_PASS_READWRITE}"
  }
}