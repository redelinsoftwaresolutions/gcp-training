#!/bin/bash
terraform apply  -var "MONGO_URL=${MONGO_URL}" \
		 -var "MONGO_PASS_READWRITE=$(cat $MONGO_PW_READWRITE)" \
		 -var "MONGO_USER_READWRITE=${MONGO_USER_READWRITE}" \
		 --auto-approve
