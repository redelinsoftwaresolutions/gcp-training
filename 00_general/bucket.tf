resource "google_storage_bucket" "redelinsoft-training-function-bucket" {
  name = "redelinsoft-training-function-bucket"
  project = "${local.project_id}"
  storage_class = "REGIONAL"
  force_destroy = "true"
  location = "${local.region}"
}
